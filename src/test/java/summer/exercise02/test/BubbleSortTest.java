package summer.exercise02.test;

import org.junit.Test;
import summer.exercise02.BubbleSort;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by phil on 2016/7/12.
 */
public class BubbleSortTest {

    @Test
    public void test_one_number_array() {
        int[] targets = new int[] {1};
        int[] numbers = new int[] {1};

        BubbleSort.sort(numbers);

        assertArrayEquals(targets, numbers);
    }

    @Test
    public void test_two_numbers_array() {
        int[] numbers = new int[] {2, 1};

        BubbleSort.sort(numbers);

        assertTrue(numbers[0] < numbers[1]);
    }

    @Test
    public void test_many_numbers_array() {
        int[] numbers = new int[] {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};

        BubbleSort.sort(numbers);

        for (int i = 0; i < numbers.length - 1; i++) {
            assertTrue(numbers[i] < numbers[i+1]);
        }
    }
}
